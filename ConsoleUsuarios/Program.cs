﻿using ConsoleUsuarios.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace ConsoleUsuarios
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://senacao.tk/objetos/usuario";

            Usuarios usuario = BuscarUsuario(url);

            Console.WriteLine("Usuario");
            Console.WriteLine(
                String.Format("Nome: {0} - Email: {1} - Telefone: {2} - Endereco: {3}",
                usuario.Nome, usuario.Email, usuario.Telefone, usuario.Endereco));
            Console.WriteLine("\n\n");
            Console.WriteLine("Conhecimentos");

            foreach (string Conhecimento in usuario.Conhecimentos)
            {
                Console.WriteLine(Conhecimento);

            }

            Console.WriteLine("\n\n");


            foreach (Qualificacao qualificacao in usuario.Qualificacoes)
            {
                Console.WriteLine(qualificacao.Nome);
                Console.WriteLine(qualificacao.Instituicao);
                Console.WriteLine(qualificacao.Ano);

            }


            Console.ReadLine();

        }


        public static Usuarios BuscarUsuario(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Console.WriteLine(content);
            Console.WriteLine("\n\n");
            Usuarios usuarios = JsonConvert.DeserializeObject<Usuarios>(content);
            return usuarios;


        }









    }
}
